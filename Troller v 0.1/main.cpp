#include"Menu.h"
#include"CarsMove.h"
#include"TrafficLights.h"
#include"Map.h"
#include <iostream>
#include<cstdlib>

using namespace std;
//***Window Size
#define ScreenWidth 1000
#define ScreenHeigth 500


void pokazMysze(int mouseX, int mouseY)
{
	cout << "mouseX:" << mouseX << " ";
	cout << "mouseY:" << mouseY << " ";
	
}

void siatka()									//rysowanie siatki
{
	for (int i = 0; i <ScreenHeigth;i+=25)
	al_draw_line(0, i, 1000, i, al_map_rgb(0, 150, 0), 1);
	for (int i = 0; i < ScreenWidth;i+=60)
	al_draw_line(i, 0, i, 500, al_map_rgb(0, 150, 0), 1);
}

AutoCars generujNowySamochod()
{
	AutoCars samochod;
	samochod.ID = 1;
	samochod.speed = 1;
	samochod.x = 0;
	samochod.y = 0;
	samochod.health = full;
	return samochod;
}
//**chceck cars system
AutoCars checkCars(AutoCars samochod, AutoCars frontSamochod);
//***check lights system
AutoCars checkLights(AutoCars samochod, TrLights Light, int stopLine);
bool Collison(AutoCars car1, AutoCars car2);

//***bitmapy
struct {										
	ALLEGRO_BITMAP *backgroundMenu;
	ALLEGRO_BITMAP *car;
	ALLEGRO_BITMAP *car2;
	ALLEGRO_BITMAP *map1;
	ALLEGRO_BITMAP *redLight;
	ALLEGRO_BITMAP *greenLight;
	ALLEGRO_BITMAP *redLightRotated;
	ALLEGRO_BITMAP *greenLightRotated;
	ALLEGRO_BITMAP *gameOver;


}graphic;
//***menu status
enum menuState { Neutral, StartGame, Settings, Rank, Exit };

int main()
{ 
#pragma region FPS,timer,inity,sprawdzanie
	const int FPS = 60;
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	bool redraw = true;
														//**** TESTY ****
	al_init();
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	timer = al_create_timer(2.0 / FPS);
	display = al_create_display(ScreenWidth, ScreenHeigth);
	al_set_window_title(display, "Troller v 0.2 by Dawid Ziółkowski @ Dside");

	if (!timer) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}


	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}


	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "failed tp create event!\n");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return-1;
	}
														//**** KONIEC TESTOW ****

	al_set_window_position(display, 200, 200);
	al_install_keyboard();
	al_install_mouse();
	al_init_font_addon();								// INIT
	al_init_primitives_addon();
	al_init_ttf_addon();
	al_init_image_addon();
#pragma endregion

#pragma region sciezki
	ALLEGRO_FONT *font18 = al_load_font("arial.ttf", 18, 0);
	ALLEGRO_FONT *font24 = al_load_font("arial.ttf", 24, 0);		
	if (!(graphic.backgroundMenu = al_load_bitmap("glowneMenu.bmp"))) std::cout << "\nERROR glowneMenu.bmp \n";
	if (!(graphic.car = al_load_bitmap("graph/car_down_to_up.png"))) std::cout << "\nERROR graph/car_down_to_up.pngp \n";
	if (!(graphic.car2 = al_load_bitmap("graph/car_left_to_right.png"))) std::cout << "\nERROR graph/car_left_to_right.png \n";
	if (!(graphic.map1 = al_load_bitmap("graph/default_map.jpg")))std::cout << "\nERROR graph/default_map.jpg \n";
	if (!(graphic.redLight = al_load_bitmap("graph/light_red.png"))) std::cout << "\nERROR graph/light_red.png \n";
	if (!(graphic.greenLight = al_load_bitmap("graph/light_green.png"))) std::cout << "\nERROR graph/light_green.png \n";
	if (!(graphic.redLightRotated = al_load_bitmap("graph/light_red_rotated.png"))) std::cout << "\nERROR graph/light_red_rotated.png \n";
	if (!(graphic.greenLightRotated = al_load_bitmap("graph/light_green_rotated.png"))) std::cout << "\nERROR graph/light_green_rotated.png \n";
	if (!(graphic.gameOver = al_load_bitmap("graph/game_over.jpg"))) std::cout << "\nERROR graph/game_over.png \n";
#pragma endregion 

#pragma region registers
	al_draw_bitmap(graphic.backgroundMenu, 0, 0, 0);
	al_flip_display();

	
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_mouse_event_source());
#pragma endregion

#pragma region zmienne
	int x = 69, y = 260 - 45;
	int x_mouse, y_mouse;
	int position = 0;
	int backCar = 72;

	bool ESC = false;
	bool options = false;
	bool playGame = false;
	bool ranking = false;
	bool changeLight = false;
//***zmienne testowe
	int p = 0, count=0;

#pragma endregion
	TrLights Light;
	Light.ID = 1;
	Light.LightColor = RED;
	
	TrLights LightLeft;
	LightLeft.ID = 2;
	LightLeft.LightColor = RED;
	

	int state = Neutral;						// 0-Default  1-StartGame  2-Settings  3-Rank  4-Exit
	bool draw = true;
	bool kolizja = 0;
	al_start_timer(timer);

	AutoCars samochodzik = generujNowySamochod();
	AutoCars samochodzik2 = generujNowySamochod();
	AutoCars samochod[1];
	samochod[0].speed = -60;
	samochod[1].speed = -60;
	while (!ESC)
	{
		
		ALLEGRO_EVENT ev;

		al_wait_for_event(event_queue, &ev);

		
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			if (state == StartGame)
			{
				p += 3;
				
				samochod[0] = checkLights(samochod[0],Light,230);
				samochod[1] = checkLights(samochod[1], LightLeft, 355);
				samochodzik = checkLights(samochodzik,  Light, 230);
				samochodzik2 = checkLights(samochodzik2, LightLeft, 355);
				if (!Collison(samochodzik2, samochodzik))
				{
					state = Neutral;
					draw = true;
					kolizja = 1;
				}
				
			}
				count++;	
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_DOWN:
				y += 45;
				position++;
				break;
			case ALLEGRO_KEY_UP:
				y -= 45;
				position--;
				break;

			case ALLEGRO_KEY_ENTER:
				if (position == 1)
				{
					state = StartGame;
					draw = false;
				}
				else if (position == 2)
				{
					state = Settings;
					draw = false;
				}
				else if (position == 3)
				{
					state = Rank;
					draw = false;
				}
				else if (position == 4)
					ESC = true;
				break;
			}

		}else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{	
			x_mouse = ev.mouse.x;
			y_mouse = ev.mouse.y;
				
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button & 1)
			{
				system("cls");
				cout << "x" << x_mouse << " ";
				cout << "y" << y_mouse << " ";
				cout << "mouseButt";	
				if ((x_mouse >= 920 && x_mouse <= 972) && (y_mouse >= 23 && y_mouse <= 38))			
				{
					state = Neutral;
					draw = true;
				}
				else if ((x_mouse >= 420 && x_mouse <= 445) && (y_mouse >= 377) && (y_mouse <= 411))
				{

					if (LightLeft.LightColor == RED)
						LightLeft.LightColor = GREEN;
					else if (LightLeft.LightColor == GREEN)
						LightLeft.LightColor = RED;
					cout << "test2";
				}
				else if ((x_mouse >= 465 && x_mouse <= 499) && (y_mouse >= 296) && (y_mouse <= 321))
				{
					if (Light.LightColor == RED)
						Light.LightColor = GREEN;
					else if (Light.LightColor == GREEN)
						Light.LightColor = RED;	
					cout << "test1";
				}
				
			}
				
		}
		
		
		switch (state)
		{
			case StartGame:
			{
				//***********************************************************menu Rozpocznij gre
				al_draw_bitmap(graphic.map1, 0, 0, 0);
				al_draw_bitmap(graphic.car, 468,  samochodzik.speed, 90);
				al_draw_bitmap(graphic.car2, samochodzik2.speed, 379, 0);
				al_draw_bitmap(graphic.car, 468, samochod[0].speed-60, 90);
				al_draw_bitmap(graphic.car2, samochod[1].speed-60 , 379, 0);
				siatka();
				al_draw_textf(font24, al_map_rgb(255, 0, 0), 5, 5, ALLEGRO_ALIGN_LEFT, "fRAME: %i", count);
				al_draw_textf(font18, al_map_rgb(0, 0, 255), 920, 40, ALLEGRO_ALIGN_LEFT, "ESC");
				al_draw_textf(font18, al_map_rgb(0, 0, 255), 920, 18, ALLEGRO_ALIGN_LEFT, "Pause");
			
				if (Light.ID == 1 && Light.LightColor == GREEN)
					al_draw_bitmap(graphic.greenLight, 465, 296, 0);
				if (Light.ID == 1 && Light.LightColor == RED)
					al_draw_bitmap(graphic.redLight, 465, 296, 0);
				
				if (LightLeft.LightColor == GREEN)
					al_draw_bitmap(graphic.greenLightRotated, 420, 377,0);
				if (LightLeft.LightColor == RED)
					al_draw_bitmap(graphic.redLightRotated, 420, 377,0);
				
				al_flip_display();
				break;
			}

			case Settings:
			{
				//************************************************************menu opitons
			/*	selectFromMenu(position);
				al_rest(1);
				options = false;*/
			}
			case Rank:
			{
				//***********************************************************menu ranking
			}
		}
		
		if (draw)
		{
			if (y > 400)
			{
				y = 260;
				position = 1;
			}
			if (y < 260)
			{
				y = 395;
				position = 4;
			}
			if (kolizja)
			{
				al_draw_bitmap(graphic.gameOver, 0, 0, 0);
		
				al_flip_display();
				al_rest(3.0);
				kolizja = 0;
			}
			al_draw_bitmap(graphic.backgroundMenu, 0, 0, 0);
			al_draw_textf(font24, al_map_rgb(255, 0, 0), 5, 5, ALLEGRO_ALIGN_LEFT, "fARME: %i", count);
			showMenu(x, y, position);
			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
		}
	}


				
		
		al_destroy_timer(timer);
		al_destroy_event_queue(event_queue);
		al_destroy_display(display);

		
	return 0;
}
AutoCars checkCars(AutoCars samochod, AutoCars frontSamochod)
{
	if ((frontSamochod.speed - 75) - samochod.speed <=20)
	{
		samochod.speed += 0;
	}
	return samochod;
}
AutoCars checkLights(AutoCars samochod, TrLights Light, int stopLine)
{
	if (Light.LightColor == GREEN || Light.LightColor == RED)
	{
		if ((Light.LightColor == RED && (samochod.speed > (stopLine - 20) && samochod.speed <stopLine)))
		{
			samochod.speed += 1;
		}
		else if ((Light.LightColor == RED && (samochod.speed > stopLine && samochod.speed < stopLine + 5)))
		{
			samochod.speed += 0;
		}else samochod.speed += 2;
	}
	return samochod;
}
bool Collison(AutoCars car1, AutoCars car2)
{
	if ((car1.speed > 431 && car1.speed < 498) && (car2.speed>305 && car2.speed < 370))
	{
		cout << "kolizja";
		al_draw_bitmap(graphic.gameOver, 0, 0, 0);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		al_flip_display();
		
		return 0;
	}else
	return 1;
}